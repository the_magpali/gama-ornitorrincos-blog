class HomeController < ApplicationController
  
  def index
    @posts = Post.all
    @lead = Lead.new
  end
  
  def download_pdf
    send_file "#{Rails.root}/app/assets/docs/6_dicas_valiosas_fidelizar_clientes_ebook.pdf", type: "application/pdf", x_sendfile: true
  end

end
