class Lead < ApplicationRecord
    validates :name, :presence => true 
    validates :email, :presence => true, :uniqueness => true
    validates :stack, :presence => true
    # validates :email, :uniqueness => true
end


